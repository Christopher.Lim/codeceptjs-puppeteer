// in this file you can append custom step methods to 'I' object
// It's like command.js in cypress
// 
module.exports = function() {
  return actor({

    // Define custom steps here, use 'this' to access default methods of I.
    // It is recommended to place a general 'login' function here.
    createTodo: function(todo) {
      this.fillField(this.fields.todoInput, todo);
      this.pressKey('Enter');
    }

  });
}
