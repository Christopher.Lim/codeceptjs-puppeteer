const { I } = inject();

module.exports = {

  // setting locatiors
  fields: {
    todoInput: '.new-todo'
  },
  deleteButton: {css: '.destroy'},

   addTodo(todo) {
     I.fillField(this.fields.todoInput, todo);
     I.pressKey('Enter');
  },
}
