const { I } = inject(); //global

Feature('Sample Test'); // Describe Block

beforeEach(() => {
  I.amOnPage('/');
});

Scenario('open my website and pauses', ({ I }) => { //it block
  // pause();
});


Scenario('call custom steps', ({ I }) => {
  I.createTodo('email', 'password');
});

Scenario.todo('this is how to call todo test');