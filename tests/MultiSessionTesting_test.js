const { I, todoPage } = inject(); //global

Feature('MultiSessionTesting');

Scenario('opening this page as multi user', ({ I }) => {
  // opens 2 additional browsers
  session('Host');
  session('Participant');

  I.amOnPage('/');

  session('Host', () => {
    I.amOnPage('/');
    I.scrollTo('input');
    todoPage.addTodo('I am a host');
  });

  session('Participant', () => {
    I.amOnPage('/');
    I.scrollTo('input');
    todoPage.addTodo('I am a participant');
  });

  session('Host', () => {
    I.see('I am a host', '.todo-list');
  });

  session('Participant', () => {
    I.see('I am a participant', '.todo-list');
  });
})
