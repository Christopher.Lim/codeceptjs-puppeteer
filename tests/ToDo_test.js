const { expect } = require('chai'); // from chai for assert
const assert = require('assert'); // assertion
const { I, classPage } = inject(); //global

Feature('ToDo');
beforeEach(() => {
  I.amOnPage('/');
});

Scenario('create todo item', async () => {
  I.dontSeeElement('.todo-count');
  I.fillField('What needs to be done?', 'Write a guide');
  I.pressKey('Enter');
  I.see('Write a guide', '.todo-list');
  I.see('1 item left', '.todo-count');
});

// The argument inside async function is how you declare
// page object that will only be used inside the test
Scenario('get value of current tasks', async ({ I, todoPage }) => {
  todoPage.addTodo('do 1');
  todoPage.addTodo('do 2');
  let numTodos = await I.grabTextFrom('.todo-count strong');
  assert.equal(2, numTodos);
  //expect(numTodos).to.equal('2');
});

Scenario('get the header of the page', async () => {
  let header = await classPage.getHeader();
  expect(header).to.equal('React');
});

Scenario('get the subheader of the page', async () => {
  let subheader = await classPage.getSubHeader(1);
  expect(subheader).to.equal('Example');
});

Scenario('get the subheader of the page in for loop', async () => {
  let headerArr = ['Example', 'React + Backbone.js', 'Scala.js + React', 'React + Alt'];

  for (let index = 1; index < headerArr.length; index++) {
    expect(await classPage.getSubHeader(index)).to.equal(headerArr[index - 1]);
  }
});

Scenario('"Within" testing', async ({todoPage}) => {
  within('.todoapp', () => {
    todoPage.addTodo('my new item');
    I.see('1 item left', '.todo-count');
    I.click('.todo-list input.toggle');
  });
  I.see('0 items left', '.todo-count');
});
