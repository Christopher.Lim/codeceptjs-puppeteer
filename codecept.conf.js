const { setHeadlessWhen } = require('@codeceptjs/configure');
const { devices } = require('playwright');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: 'tests/*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://todomvc.com/examples/react',
      show: true,
      browser: 'chromium',
      waitForNavigation: 'networkidle0',
      waitForAction: 500,
      emulate: { isMobile: true, deviceScaleFactor: 2 }
    }
  },
  include: {
    I: './pages/custom_steps.js',//injects actor
    todoPage: './pages/TodoPage.js', //pageObject for todo
    classPage: './pages/ClassPage.js'
  },
  bootstrap: null,
  teardown: null,
  mocha: {},
  name: 'codeceptjs-playwright',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}